xfce4-indicator-plugin (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1
  * d/p/01_ayatana rebased for new upstream
  * d/control: update standards version to 4.6.0

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 11 Jan 2022 16:33:51 +0100

xfce4-indicator-plugin (2.4.0-1) unstable; urgency=medium

  * Team upload.
  * d/watch: Update for mirrorbit and use uscan special strings.
  * New upstream version 2.4.0.
    - Refresh patch.
  * d/compat, d/control:
    - Drop d/compat in favor of debhelper-compat, bump to 12.
  * d/control:
    - Drop gtk-doc-tools and libexo-1-dev from Build-Depends. (Closes: #978680)
    - Update 'Homepage' field, goodies.xfce → docs.xfce.
    - R³: no.
  * d/rules: Drop autoreconf override, no longer needed.
  * Update Standards-Version to 4.5.1.

 -- Unit 193 <unit193@debian.org>  Tue, 12 Jan 2021 20:30:45 -0500

xfce4-indicator-plugin (2.3.4-2) unstable; urgency=medium

  * d/gbp.conf added, following DEP-14
  * d/control: drop Lionel from uploaders, thanks for the work
  * d/control: update package name for libayatana-ido b-dep (closes: #907376)

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 30 Sep 2018 13:53:58 +0200

xfce4-indicator-plugin (2.3.4-1) unstable; urgency=medium

  [ Unit 193 ]
  * New upstream release.
    - Refresh and extend patch to cover another libapplication.so rename.
  * Bump Standards-Version to 4.1.3.
  * d/control:
    - Moved the package to git on salsa.debian.org
    - Updated the maintainer address to debian-xfce@lists.debian.org
  * d/copyright, d/watch: Use https where possible.
  * d/changelog: Remove trailing whitespace in old entries.

  [ Mike Gabriel ]
  * debian/patches: Fix 01_ayatana.patch: (Closes: #893639)
    + Adapt INDICATOR_SERVICE_DIR to /usr/share/ayatana/indicators.
    + Adapt indicator service and .so names in indicator-dialog.c
    + Use macro HAVE_LIBAYATANA_INDICATOR_INDICATOR_NG_H instead of
      HAVE_LIBINDICATOR_INDICATOR_NG_H to check that NG indicators are
      supported.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 28 Mar 2018 22:49:25 +0200

xfce4-indicator-plugin (2.3.3-1) unstable; urgency=medium

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org
  * Add Build-Depends on libgtk-3-dev (>= 3.6).
  * Tighten Build-Depends on libxfce4ui-2-dev and libxfce4util-dev.

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/control
    - update xfce4-panel-dev build-dep to libxfce4-panel-2.0-dev.
    - update libindicator-dev build-dep to libindicator3-dev and version it.

  [ Mateusz Łukasik ]
  * New upstream release.

  [ Unit 193 ]
  * Run wrap-and-sort.
  * Bump Standards-Version to 4.1.1.
  * d/control, d/copyright: Use https where possible.
  * d/compat, d/control: Bump dh compat to 10.
  * d/control:
    - B-D on gtk-doc-tools and xfce4-dev-tools for autoreconf.
    - Drop B-D on autotools-dev and versioned B-D on dpkg-dev.
    - Update B-D for Ayatana.
      + libido3-0.4-dev → libayatana-ido3-0.4-dev
      + libindicator3-dev → libayatana-indicator3-dev
    - Drop recommend on indicator-messages-gtk2.
    - Recommend ayatana-indicator-application.
  * d/p/01_ayatana.patch:
    - Update configure.ac and headers to look for ayatana libs.
  * d/rules:
    - Drop explicit --parallel and --with autotools-dev.
    - override_dh_autoreconf to use xdt-autogen.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 15 Oct 2017 16:32:43 +0200

xfce4-indicator-plugin (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - add build-dep on libxfce4ui-1-dev.
  * debian/rules:
    - don't ship .la files.
    - ignore plugin for dh_makeshlibs, since it's a plugin.
    - use autotools-dev addon to update config.{guess,sub}.
  * debian/control:
    - add build-dep on dpkg-dev for including pkg-info.mk.
    - add build-dep on autotools-dev

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 10 Jun 2013 22:22:42 +0200

xfce4-indicator-plugin (0.5.0-2) unstable; urgency=low

  [ Lionel Le Folgoc ]
  * debian/control: drop b-dep on libxfcegui4-dev, unused.

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.4.
    - drop useless versions in build-deps
    - drop dpkg-dev build-dep
  * debian/rules:
    - enable all hardening flags.

  [ Evgeni Golov ]
  * Update d/copyright to copyright-format/1.0

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 25 May 2013 19:48:16 +0200

xfce4-indicator-plugin (0.5.0-1) unstable; urgency=low

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/rules:
    - build with --parallel.
  * debian/control:
    - update debhelper build-dep to 9.
    - add build-dep on exo and xfconf.
    - update standards version to 3.9.3.

 -- Evgeni Golov <evgeni@debian.org>  Sat, 05 May 2012 15:54:41 +0200

xfce4-indicator-plugin (0.4.0-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Switch Recommends to indicator-messages-gtk2, we cannot use
    the GTK3 version provided by indicator-messages.

  [ Lionel Le Folgoc ]
  * New upstream release.
    - "Scrolling the mousewheel over the sound-indicator doesn't adjust the
      volume". lp: #879928
    - "xfce4-indicator-plugin does not update output". lp: #852017
  * debian/patches:
    - search-for-indicator-0.4-pc.patch, fix_menu_position.patch,
      attach_button_to_menu_right_at_the_beginning.patch: dropped, included
      upstream.
    - potfiles.patch: dropped, not needed anymore.

  [ Yves-Alexis Perez ]
  * debian/rules:
    - enable hardening flags.
  * debian/compat bumped to 9.
  * debian/control:
    - add build-dep on dpkg-dev 1.16.1 for hardening support.
    - update debhelper build-dep to 8.9.4 for hardening support.

 -- Evgeni Golov <evgeni@debian.org>  Sun, 19 Feb 2012 15:08:18 +0100

xfce4-indicator-plugin (0.3.1-3) experimental; urgency=low

  * Add search-for-indicator-0.4-pc.patch from Ubuntu,
    libindicator-dev moved the .pc file.
  * Build-Depend on libindicator-dev (>=0.3.90)
  * Bump debian/compat to 9 and Build-Depend on debhelper >= 8.9.0~
    for new-style buildflags.
    Drop the old ones from debian/rules.

 -- Evgeni Golov <evgeni@debian.org>  Tue, 18 Oct 2011 16:15:17 +0200

xfce4-indicator-plugin (0.3.1-2) unstable; urgency=low

  * Add fix_menu_position.patch and
    attach_button_to_menu_right_at_the_beginning.patch
    as suggested by upstream.

 -- Evgeni Golov <evgeni@debian.org>  Sun, 04 Sep 2011 22:43:47 +0200

xfce4-indicator-plugin (0.3.1-1) unstable; urgency=low

  * New upstream release.
    + Supports transparent background                               LP:#778998

 -- Evgeni Golov <evgeni@debian.org>  Fri, 15 Jul 2011 15:27:20 +0200

xfce4-indicator-plugin (0.2.1-1) unstable; urgency=low

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 06 May 2011 22:33:52 +0200

xfce4-indicator-plugin (0.2.0-2) unstable; urgency=low

  [ Lionel Le Folgoc ]
  * Upload to unstable.
  * debian/control:
    - add myself to Uploaders.
    - remove Simon and Emanuele from uploaders, thanks to them.
    - bump xfce4-panel-dev b-dep to (>= 4.8.0).

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.2.

 -- Lionel Le Folgoc <mrpouit@gmail.com>  Tue, 19 Apr 2011 23:02:15 +0200

xfce4-indicator-plugin (0.2.0-1) experimental; urgency=low

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/control:
    - add build-dep on hardening-includes
    - update standards version to 3.9.1.
  * debian/rules:
    - pick {C,LD}FLAGS from dpkg-buildflags
    - add -O1, --as-needed and -z,defs to LDFLAGS
    - add hardening flags to {C,LD}FLAGS
  * debian/watch updated.
  * debian/patches:
    - 01_libindicator0.3.0_compat dropped, included upstream.
    - 02_menu_on_no-indicators as well.
    - 03_skip_pofiles_in_patches removed, now useless.

  [ Evgeni Golov ]
  * debian/copyright:
    - Update years.
    - Add Yves-Alexis.
    - Fix upstream URL.

 -- Evgeni Golov <evgeni@debian.org>  Sun, 27 Feb 2011 14:44:16 +0100

xfce4-indicator-plugin (0.0.1-2) unstable; urgency=low

  * debian/control:
    + Add Provides: indicator-renderer
    + Add Recommends: indicator-messages
  * debian/patches/02_menu_on_no-indicators.patch
    + Display a menu even when there are no indicators.        closes: #587167
  * debian/patches/03_skip_pofiles_in_patches.patch
    + Split the POTFILES.skip patch out, list all changed files in there.

 -- Evgeni Golov <evgeni@debian.org>  Wed, 14 Jul 2010 09:35:08 +0200

xfce4-indicator-plugin (0.0.1-1) unstable; urgency=low

  * Initial release                                            closes: #534555

 -- Evgeni Golov <evgeni@debian.org>  Sun, 13 Jun 2010 17:14:30 +0200
